//alert("Hi");

//Mock DB
let posts = [];

// Posts ID
let count = 1;

//Add post - eventListener
// This will trigger an event  that will add a new post in out mock database upon clicking the "Create" button

document.querySelector("#form-add-post").addEventListener("submit", (e) => {
	e.preventDefault()
	posts.push({
		id:count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	})
	// count will increment everytime a new posts is added
	count++
	console.log(posts);
	alert("Successfully added!");
	showPosts();
});

const showPosts = () => {
	//Create a variable that will contain all the posts.
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
		<div id="post-${post.id}">
		<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>

		</div>`
	});
	console.log(postEntries);

	document.querySelector("#div-post-entries").innerHTML = postEntries
}

// Edit Post Button
// We will create a function that will be called in the onClick() event and will pass the value in the Update form input box

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// Pass the id, title, and body from the post to be updated in the Edit Post Form
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
}

// Update post - eventListener
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
	e.preventDefault();

	for (let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value

			showPosts(posts);
			alert("Successfully Updated");
			break;
		}
	}
})

// ACTIVITY - DELETE

const deletePost = (id) => {
	const del = posts.findIndex(object => {
		return object.id === id
	})
	//console.log(del + 1)
	posts.splice(del, 1)
	showPosts()
}


